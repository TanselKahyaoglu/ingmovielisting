//
//  NavigationHelper.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation
import UIKit

enum Page: String {
    case Main = "MainPageVC"
    case Detail = "DetailPageVC"
}

enum AppStoryboard: String {
    case Main = "Main"
}

class NavigationHelper {
    
    class func push(fromVC: UIViewController,
                    appStoryboard: AppStoryboard? = nil,
                    page: Page,
                    data: BaseViewData,
                    animated: Bool = true) {
        var storyboard: UIStoryboard?
        if let appSB = appStoryboard {
            storyboard = UIStoryboard(name: appSB.rawValue, bundle: nil)
        } else if let vcSB = fromVC.storyboard {
            storyboard = vcSB
        }
        guard let sb = storyboard,
              let destinationVC = sb.instantiateViewController(withIdentifier: page.rawValue) as? BaseNavigableVC else { return }
        destinationVC.viewData = data
        fromVC.navigationController?.pushViewController(destinationVC, animated: animated)
    }

}
