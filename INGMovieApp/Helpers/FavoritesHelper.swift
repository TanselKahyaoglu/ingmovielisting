//
//  FavoritesHelper.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

class FavoritesHelper {
    
    private static let favoriteKey = "UserFavoriteKey"
    
    class func saveFavorite(id: Int) {
        var arr = UserDefaults.getIntArray(key: favoriteKey)
        arr.append(id)
        UserDefaults.saveIntArray(ids: arr, key: favoriteKey)
    }
    
    class func removeFavorite(id: Int) {
        var arr = UserDefaults.getIntArray(key: favoriteKey)
        arr.removeAll(where: { $0 == id })
        UserDefaults.saveIntArray(ids: arr, key: favoriteKey)
    }
    
    class func checkIsFavorited(id: Int) -> Bool {
        let arr = UserDefaults.getIntArray(key: favoriteKey)
        return arr.contains(id)
    }
    
}
