//
//  LoadingIndicatorHelper.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation
import UIKit

class LoadingHelper {

    static let shared = LoadingHelper()

    private let loadingIndicatorView = LoadingIndicatorView.instantiateNib()

    var isShowing: Bool = false

    func show(in vc: UIViewController) {
        if !isShowing {
            isShowing = true
            DispatchQueue.main.async { [weak self] in
                guard let indicatorView = self?.loadingIndicatorView else { return }
                vc.view.addSubview(indicatorView)
                vc.view.bringSubviewToFront(indicatorView)
            }
        }
    }

    func hide() {
        DispatchQueue.main.async { [weak self] in
            self?.isShowing = false
            self?.loadingIndicatorView?.removeFromSuperview()
        }
    }

}
