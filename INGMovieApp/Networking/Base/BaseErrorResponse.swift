//
//  BaseErrorResponse.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 3.12.2020.
//

import Foundation

struct BaseErrorResponse: Codable {
    
    let statusCode: Int
    let statusMessage: String
    let success: Bool
    
}
