//
//  Networking.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 3.12.2020.
//

import Foundation

class Networking {
        
    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    class func request<S: Decodable>(method: HTTPMethod,
                                     path: String,
                                     urlParameters: [String: String]? = nil,
                                     succeed: @escaping (S) -> Void,
                                     failed: @escaping (BaseErrorResponse) -> Void) {
        
        guard var components = URLComponents(string: ServiceSettings.baseUrl + path) else { return }
        if let urlParameters = urlParameters {
            components.queryItems = urlParameters.map {
                URLQueryItem(name: $0.key, value: $0.value)
            }
        } else {
            components.queryItems = []
        }
       
        components.queryItems?.append(URLQueryItem(name: "api_key", value: ServiceSettings.apiKey))
        components.queryItems?.append(URLQueryItem(name: "language", value: "en"))

        guard let url = components.url else { return }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        URLSession.shared.dataTask(with: request,
                                   completionHandler: { data, response, error in
                                    if let error = error {
                                        DispatchQueue.main.async {
                                            failed(BaseErrorResponse(statusCode: -1,
                                                                     statusMessage: error.localizedDescription,
                                                                     success: false))
                                        }
                                    }
                                    if let status = response as? HTTPURLResponse {
                                        guard let data = data else { return }
                                        print("Response Code: \(status.statusCode)")
                                        if status.statusCode >= 200 && status.statusCode <= 299 {
                                            do {
                                                let decoder = try getJsonDecoder().decode(S.self, from: data)
                                                DispatchQueue.main.async {
                                                    succeed(decoder)
                                                }
                                            } catch {
                                                DispatchQueue.main.async {
                                                    failed(BaseErrorResponse(statusCode: status.statusCode,
                                                                             statusMessage: "Decode Error",
                                                                             success: false))
                                                }
                                            }
                                        } else {
                                            DispatchQueue.main.async {
                                                do {
                                                    let decoded = try getJsonDecoder().decode(BaseErrorResponse.self, from: data)
                                                    failed(decoded)
                                                } catch {
                                                    failed(BaseErrorResponse(statusCode: status.statusCode, statusMessage: "", success: false))
                                                }
                                            }
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            failed(BaseErrorResponse(statusCode: -1, statusMessage: "", success: false))
                                        }
                                    }
        }).resume()
    }
    
    private class func getJsonDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
}
