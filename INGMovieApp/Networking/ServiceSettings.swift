//
//  ServiceSettings.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 3.12.2020.
//

import Foundation

struct ServiceSettings {
    
    static let baseUrl = "https://api.themoviedb.org"
    static let imageBaseUrl = "https://image.tmdb.org"
    static let apiKey = "fd2b04342048fa2d5f728561866ad52a"
    
}

struct Endpoints {
    
    static let wideImagePath = "/t/p/w780"
    static let narrowImagePath = "/t/p/w200"
    static let getPopularMovies = "/3/movie/popular"
    static let movieDetail = "/3/movie"

}

