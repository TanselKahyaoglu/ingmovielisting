//
//  MovieDetailResponse.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

struct MovieDetailResponse: Decodable {
    
    let id: Int
    let title: String
    let overview: String
    let voteCount: Int
    let posterPath: String
    
}
