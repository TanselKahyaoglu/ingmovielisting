//
//  GetMoviesResponse.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 4.12.2020.
//

import Foundation

struct GetMoviesResponse: Decodable {
    
    let page: Int
    let totalPages: Int
    let results: [MovieListModel]
    let totalResults: Int
    
}
