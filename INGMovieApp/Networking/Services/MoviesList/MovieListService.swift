//
//  MovieListService.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 4.12.2020.
//

import Foundation

class MovieListService {
    
    func getMovieList(page: Int,
                      onSuccess: @escaping Handler<GetMoviesResponse>,
                      onFailure: @escaping Handler<BaseErrorResponse>) {
        let parameters = ["page": "\(page)"]
        Networking.request(method: .get,
                           path: Endpoints.getPopularMovies,
                           urlParameters: parameters,
                           succeed: onSuccess,
                           failed: onFailure)
    }
    
    func getMovieDetail(id: Int,
                        onSuccess: @escaping Handler<MovieDetailResponse>,
                        onFailure: @escaping Handler<BaseErrorResponse>) {
        let path = Endpoints.movieDetail + "/\(id)"
        Networking.request(method: .get,
                           path: path,
                           succeed: onSuccess,
                           failed: onFailure)
    }
    
}
