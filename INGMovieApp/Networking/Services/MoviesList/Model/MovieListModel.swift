//
//  MovieListModel.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 4.12.2020.
//

import Foundation

struct MovieListModel: Decodable {
    
    let adult: Bool
    let backdropPath: String?
    let genreIds: [Int]
    let id: Int
    let originalLanguage: String
    let originalTitle: String
    let overview: String
    let popularity: Double
    let posterPath: String
    let releaseDate: String?
    let title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int
    
}
