//
//  MainPageBaseCell.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 4.12.2020.
//

import Foundation
import UIKit


class MainPageBaseCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    
    //MARK: Handlers
    var onFavoriteTapped: Handler<Bool>?
    
    //MARK: Variables
    var movie: MovieListModel? {
        didSet {
            guard let movie = movie else { return }
            configureCell(movie: movie)
        }
    }
    
    var isFavorited: Bool = false {
        didSet {
            configureFavoriteButton()
        }
    }
    
    //MARK: Configure UI
    func configureCell(movie: MovieListModel) {
        lblTitle.text = movie.title
        setImage(path: movie.posterPath)
    }
    
    func setImage(path: String) { }
    
    private func configureFavoriteButton() {
        if isFavorited {
            btnFav.setBackgroundImage(#imageLiteral(resourceName: "star-filled"), for: .normal)
        } else {
            btnFav.setBackgroundImage(#imageLiteral(resourceName: "star"), for: .normal)
        }
    }
    
    //MARK: Actions
    @IBAction func actionFav(_ sender: Any) {
        onFavoriteTapped?(isFavorited)
        isFavorited = !isFavorited
        configureFavoriteButton()
    }
    
    //MARK: For Reuse
    override func prepareForReuse() {
        imgView.image = nil
        lblTitle.text = nil
    }
    
}

