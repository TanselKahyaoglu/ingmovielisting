//
//  MainPageGridCell.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation
import UIKit

class MainPageGridCell: MainPageBaseCell {
    
    override func setImage(path: String) {
        imgView.set(with: ServiceSettings.imageBaseUrl + Endpoints.narrowImagePath + path)
    }
    
}
