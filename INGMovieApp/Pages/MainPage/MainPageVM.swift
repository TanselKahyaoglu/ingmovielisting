//
//  MainPageVM.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation

protocol MainPageVMDelegate {
    func moviesLoaded()
    func movieDetailLoaded(detail: MovieDetailResponse)
    func showLoading()
    func hideLoading()
}

class MainPageVM: BaseVM {
    
    //MARK: Variables
    var service: MovieListService?
    var delegate: MainPageVMDelegate?
    
    private var page = 1
    private var movies = [MovieListModel]()
    private var movieDataSource = [MovieListModel]()
    private var isSearchActive: Bool = false
    private var hasMoreOnService: Bool = true
    
    private var isServiceLoading: Bool = false {
        didSet {
            if isServiceLoading {
                delegate?.showLoading()
            } else {
                delegate?.hideLoading()
            }
        }
    }
    
    //MARK: Init
    init(service: MovieListService) {
        super.init()
        self.service = service
    }
    
    required init() {
        super.init()
        self.service = MovieListService()
    }
    
    //MARK: Paging Methods
    func nextPage() {
        if isServiceLoading || !hasMoreOnService || isSearchActive { return }
        page += 1
        getPopularMovies()
    }
    
    //MARK: Search Options
    func search(with keyword: String) {
        movieDataSource.removeAll()
        isSearchActive = !keyword.isEmpty
        if isSearchActive {
            movieDataSource.append(contentsOf: movies.filter( { $0.title.lowercased().contains(keyword.lowercased()) }))
        } else {
            movieDataSource.append(contentsOf: movies)
        }
        delegate?.moviesLoaded()
    }
    
    //MARK: Favorite Options
    func addMovieToFavorites(movie: MovieListModel) {
        FavoritesHelper.saveFavorite(id: movie.id)
    }
    
    func removeMovieFromFavorites(movie: MovieListModel) {
        FavoritesHelper.removeFavorite(id: movie.id)
    }
    
    //MARK: Service Calls & Response Handlers
    func getPopularMovies() {
        isServiceLoading = true
        service?.getMovieList(page: page,
                              onSuccess: handleResponseSuccess,
                              onFailure: handleResponseFailure)
    }
    
    private func handleResponseSuccess(_ response: GetMoviesResponse) {
        movies.append(contentsOf: response.results)
        movieDataSource.removeAll()
        movieDataSource.append(contentsOf: movies)
        hasMoreOnService = response.totalPages != page
        delegate?.moviesLoaded()
        isServiceLoading = false
    }
    
    private func handleResponseFailure(_ error: BaseErrorResponse) {
        print(error)
        isServiceLoading = false
    }
    
    func getMovieDetail(at indexPath: IndexPath) {
        isServiceLoading = true
        service?.getMovieDetail(id: movieDataSource[indexPath.item].id,
                                onSuccess: handleDetailResponseSuccess,
                                onFailure: handleResponseFailure)
    }
    
    private func handleDetailResponseSuccess(_ response: MovieDetailResponse) {
        isServiceLoading = false
        delegate?.movieDetailLoaded(detail: response)
    }
    
    //MARK: DataSource Operations
    func getMovieCount() -> Int {
        return movieDataSource.count
    }
    
    func getMovie(at indexPath: IndexPath) -> MovieListModel {
        return movieDataSource[indexPath.item]
    }
    
}
