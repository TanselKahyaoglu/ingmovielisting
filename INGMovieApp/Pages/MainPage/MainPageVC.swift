//
//  MainPageVC.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation
import UIKit

class MainPageVC: BaseVC<MainPageVM> {
    
    //MARK: Private Enums
    private enum ListingType {
        case List
        case Grid
    }
    
    //MARK: IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var searchBar: UISearchBar!
    
    //MARK: Views
    private var btnListType: NavigationBarButton?
    
    //MARK: Variables
    private var listingType: ListingType = .List {
        didSet {
            configureListTypeButton()
        }
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        viewModel?.delegate = self
        viewModel?.getPopularMovies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }
    
    //MARK: Configure Views
    override func setNavigationTitle() -> String {
        return "Main Page".localized()
    }
    
    override func getRightBarButtons() -> [UIBarButtonItem]? {
        btnListType = NavigationBarButton(image: #imageLiteral(resourceName: "list"),
                                          target: self,
                                          action: #selector(actionListType))
        guard let btnListType = btnListType else { return nil }
        return [btnListType]
    }
    
    private func configureListTypeButton() {
        guard let btnListType = btnListType else { return }
        switch listingType {
        case .Grid:
            btnListType.setImage(image: #imageLiteral(resourceName: "grid"))
        case .List:
            btnListType.setImage(image: #imageLiteral(resourceName: "list"))
        }
    }
    
    //MARK: CollectionView Operations
    private func registerCells() {
        collectionView.register(MainPageListCell.self)
        collectionView.register(MainPageGridCell.self)
    }
    
    //MARK: Page Navigations
    private func openDetailPage(detail: MovieDetailResponse) {
        NavigationHelper.push(fromVC: self,
                              page: .Detail,
                              data: DetailPageData(detailModel: detail))
    }
    
    //MARK: Button Actions
    @objc
    private func actionListType() {
        switch listingType {
        case .List:
            listingType = .Grid
        case .Grid:
            listingType = .List
        }
        collectionView.reloadData()
    }
}

//MARK: VM Delegates
extension MainPageVC: MainPageVMDelegate {
    
    func moviesLoaded() {
        collectionView.reloadData()
    }
    
    func movieDetailLoaded(detail: MovieDetailResponse) {
        openDetailPage(detail: detail)
    }
    
    func showLoading() {
        showLoadingIndicator()
    }
    
    func hideLoading() {
        hideLoadingIndicator()
    }
}

//MARK: CollectionViewDelegates
extension MainPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.getMovieCount() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch listingType {
        case .Grid:
            return CGSize(width: (UIScreen.main.bounds.width / 2) - 20, height: 250)
        case .List:
            return CGSize(width: UIScreen.main.bounds.width - 20, height: 500)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let movie = viewModel?.getMovie(at: indexPath) else { return UICollectionViewCell() }
        var cell: MainPageBaseCell?
        switch listingType {
        case .Grid:
            cell = collectionView.dequeuCell(MainPageGridCell.self, indexPath: indexPath)
        case .List:
            cell = collectionView.dequeuCell(MainPageListCell.self, indexPath: indexPath)
        }
        if let movieCell = cell {
            movieCell.movie = movie
            movieCell.isFavorited = FavoritesHelper.checkIsFavorited(id: movie.id)
            movieCell.onFavoriteTapped = { [weak self] favorited in
                if favorited {
                    self?.viewModel?.removeMovieFromFavorites(movie: movie)
                } else {
                    self?.viewModel?.addMovieToFavorites(movie: movie)
                }
            }
            return movieCell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (viewModel?.getMovieCount() ?? 0) - 1 == indexPath.item {
            viewModel?.nextPage()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel?.getMovieDetail(at: indexPath)
    }
    
}

extension MainPageVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.search(with: "")
        searchBar.text = ""
        searchBar.endEditing(true)
        searchBar.showsCancelButton = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.search(with: searchBar.text ?? "")
        searchBar.endEditing(true)
        searchBar.showsCancelButton = true
    }

}
