//
//  DetailPageVM.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

class DetailPageVM: BaseVM {
    
    //MARK: Variables
    var movieDetail: MovieDetailResponse?

    //MARK: Favorite Operations
    
    func updateFavoriteStatus() {
        if isMovieFavorited() {
            removeMovieFromFavorites()
        } else {
            addMovieToFavorites()
        }
    }
    
    private func addMovieToFavorites() {
        guard let id = movieDetail?.id else { return }
        FavoritesHelper.saveFavorite(id: id)
    }
    
    private func removeMovieFromFavorites() {
        guard let id = movieDetail?.id else { return }
        FavoritesHelper.removeFavorite(id: id)
    }
    
    func isMovieFavorited() -> Bool {
        guard let id = movieDetail?.id else { return false }
        return FavoritesHelper.checkIsFavorited(id: id)
    }
    
    //MARK: Model Operations
    func getMovieTitle() -> String {
        return movieDetail?.title ?? ""
    }
    
    func getMovieOverview() -> String {
        return movieDetail?.overview ?? ""
    }
    
    func getVoteCount() -> Int {
        return movieDetail?.voteCount ?? 0
    }
    
    func getImageUrl() -> String {
        return ServiceSettings.imageBaseUrl + Endpoints.wideImagePath + (movieDetail?.posterPath ?? "")
    }
    
}
