//
//  DetailPageVC.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation
import UIKit

class DetailPageData: BaseViewData {

    let detailModel: MovieDetailResponse
    
    init(detailModel: MovieDetailResponse) {
        self.detailModel = detailModel
    }

}

class DetailPageVC: BaseVC<DetailPageVM> {
    
    //MARK: IBOutlets
    @IBOutlet private weak var imgView: UIImageView!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblOverview: UILabel!
    @IBOutlet private weak var lblVoteCount: UILabel!
    
    //MARK: Views
    private var btnFavorite: NavigationBarButton?
    
    //MARK: Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = viewData as? DetailPageData {
            viewModel?.movieDetail = data.detailModel
        }
        configurePage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureFavoriteButton()
    }
    
    //MARK: Configure View
    override func setNavigationTitle() -> String {
        return "Details".localized()
    }
    
    private func configurePage() {
        imgView.set(with: viewModel?.getImageUrl() ?? "")
        lblTitle.text = viewModel?.getMovieTitle()
        lblOverview.text = viewModel?.getMovieOverview()
        lblVoteCount.text = "\("Vote Count".localized()): \(viewModel?.getVoteCount() ?? 0)"
    }
    
    private func configureFavoriteButton() {
        if viewModel?.isMovieFavorited() ?? false {
            btnFavorite?.setImage(image: #imageLiteral(resourceName: "star-filled"))
        } else {
            btnFavorite?.setImage(image: #imageLiteral(resourceName: "star"))
        }
    }
    
    override func getRightBarButtons() -> [UIBarButtonItem]? {
        btnFavorite = NavigationBarButton(image: #imageLiteral(resourceName: "star"),
                                          target: self,
                                          action: #selector(actionFavorite))
        guard let btnFavorite = btnFavorite else { return nil }
        return [btnFavorite]
    }
    
    //MARK: Actions
    @objc
    private func actionFavorite() {
        viewModel?.updateFavoriteStatus()
        configureFavoriteButton()
    }
    
}
