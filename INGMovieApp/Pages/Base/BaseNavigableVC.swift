//
//  BaseNavigableVC.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation
import UIKit

protocol BaseViewData { } //For passing data between navigation controllers.

class BaseNavigableVC: UIViewController {

    var viewData: BaseViewData?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }

    //MARK: Configure Navigation Bar
    private func configureNavigationBar() {
        title = setNavigationTitle()
        configureNavigationButtons()
    }

    func setNavigationTitle() -> String {
        return ""
    }
    
    private func configureNavigationButtons() {
        navigationItem.rightBarButtonItems = getRightBarButtons()
        navigationItem.leftBarButtonItems = getLeftBarButtons()
    }
    
    func getLeftBarButtons() -> [UIBarButtonItem]? {
        return nil
    }
    
    func getRightBarButtons() -> [UIBarButtonItem]? {
        return nil
    }
    
}
