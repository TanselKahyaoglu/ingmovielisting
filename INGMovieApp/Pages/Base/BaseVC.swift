//
//  BaseVC.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation
import UIKit

class BaseVC <T: BaseVM>: BaseNavigableVC {
    
    var viewModel: T?

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        viewModel = T() 
    }
    
    func showLoadingIndicator() {
        LoadingHelper.shared.show(in: self)
    }
    
    func hideLoadingIndicator() {
        LoadingHelper.shared.hide()
    }

}
