//
//  String+Localization.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation

extension String {
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}
