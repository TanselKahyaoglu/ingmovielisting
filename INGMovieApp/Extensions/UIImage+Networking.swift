//
//  UIImage+Networking.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 4.12.2020.
//

import Foundation
import UIKit

extension UIImageView {
    
    func set(with urlString: String) {
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse,
                  httpURLResponse.statusCode == 200,
                let _ = response?.mimeType,
                let data = data,
                error == nil,
                let image = UIImage(data: data) else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
}
