//
//  UserDefaults+Custom.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

extension UserDefaults {
    
    class func saveIntArray(ids: [Int], key: String) {
        self.standard.setValue(ids, forKey: key)
    }
    
    class func getIntArray(key: String) -> [Int] {
        return self.standard.array(forKey: key) as? [Int] ?? []
    }

}
