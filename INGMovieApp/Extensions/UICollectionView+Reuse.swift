//
//  UICollectionView+Reuse.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func register(_ cellClass: AnyClass) {
        register(UINib(nibName: String(describing: cellClass.self),
                       bundle: nil),
                 forCellWithReuseIdentifier: String(describing: cellClass.self))
    }

    func dequeuCell<T: UICollectionViewCell>(_ cellClass: T.Type, indexPath: IndexPath) -> T {
        let cell = dequeueReusableCell(withReuseIdentifier: String(describing: cellClass.self), for: indexPath) as? T
        return cell ?? T()
    }
    
}
