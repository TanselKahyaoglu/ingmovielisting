//
//  Typealias.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

typealias Handler<T> = ((T) -> ())
