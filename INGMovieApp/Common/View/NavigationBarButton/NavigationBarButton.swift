//
//  NavigationBarButton.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 2.12.2020.
//

import Foundation
import UIKit

class NavigationBarButton: UIBarButtonItem {
    
    init(image: UIImage?, target: AnyObject?, action: Selector) {
        super.init()
        setImage(image: image)
        style = .plain
        self.target = target
        self.action = action
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setImage(image: UIImage?) {
        self.image = image?.withRenderingMode(.alwaysOriginal)

    }

}
