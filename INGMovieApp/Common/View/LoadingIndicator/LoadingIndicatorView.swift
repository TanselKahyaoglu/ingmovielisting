//
//  LoadingIndicatorView.swift
//  INGMovieApp
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation
import UIKit

class LoadingIndicatorView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
}
