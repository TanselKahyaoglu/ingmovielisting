# ING Movie iOS Application

## About The Project

This project developed for ING Bank iOS Developer application. In this project, I used TheMovieDB Api service and for detailed information, you can look at [Documentation](https://developers.themoviedb.org/3).
There is no 3rd party library and all code is developed with native swift language. 

- The project includes followings:
  - MVVM architecture
  - Decodable & Encodable
  - Unit Tests


## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

- Swift 5
- iOS 9.0
- Xcode 12.2

### Installation

1. Clone the repo
```sh
git clone https://TanselKahyaoglu@bitbucket.org/TanselKahyaoglu/ingmovielisting.git
```
2. Open INGMovieApp.xcodeproj file with Xcode

3. Run project on your similator

4. (Optional) For running on Real device, you should have Apple Developer account. After setting developer account, you should change Bundle Identifier to your project and set up Signing & Capabilities part.

### Images
[Main Page List](https://bitbucket.org/TanselKahyaoglu/ingmovielisting/src/master/Images/mainpagelist.png)   
[Main Page Grid](https://bitbucket.org/TanselKahyaoglu/ingmovielisting/src/master/Images/mainpagegrid.png)    
[Detail](https://bitbucket.org/TanselKahyaoglu/ingmovielisting/src/master/Images/detail.png)   

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Tansel Kahyaoğlu   

[Mail](mailto:tanselkahyaoglu@me.com)

[Linkedin](https://linkedin.com/in/tanselkahyaoglu)


