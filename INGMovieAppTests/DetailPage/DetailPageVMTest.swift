//
//  DetailPageVMTest.swift
//  INGMovieAppTests
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import XCTest

@testable import INGMovieApp

class DetailPageVMTest: XCTestCase {

    //MARK: Variables
    var viewModel: DetailPageVM?
    
    override func setUp() {
        super.setUp()
        viewModel = DetailPageVM()
        viewModel?.movieDetail = JSONLoader.getMovieDetail()
    }

    func testInit() {
        XCTAssertNotNil(viewModel)
    }
    
    func testTitle() {
        XCTAssertFalse(viewModel?.getMovieTitle().isEmpty ?? true)
    }
    
    func testOverview() {
        XCTAssertFalse(viewModel?.getMovieOverview().isEmpty ?? true)
    }
    
    func testImageUrl() {
        XCTAssertFalse(viewModel?.getImageUrl().isEmpty ?? true)
    }
    
    func testVoteCount() {
        XCTAssertFalse(viewModel?.getVoteCount() == 0)
    }
    
    func testAddFavorite() {
        viewModel?.updateFavoriteStatus()
        XCTAssertTrue(viewModel?.isMovieFavorited() ?? false)
    }
    
    func testRemoveFavorite() {
        viewModel?.updateFavoriteStatus()
        XCTAssertFalse(viewModel?.isMovieFavorited() ?? true)
    }

}
