//
//  JSONLoader.swift
//  INGMovieAppTests
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

@testable import INGMovieApp

class JSONLoader {
    
    class func load(fileName: String) -> Data {
        if let path = Bundle(for: self).path(forResource: fileName, ofType: "json") {
            do {
                return try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            } catch {
                return Data()
            }
        }
        return Data()
    }
    
    class func getMovieDetail() -> MovieDetailResponse? {
        do {
            return try CustomJSONDecoder.decoder().decode(MovieDetailResponse.self, from: JSONLoader.load(fileName: "MovieDetail"))
        } catch {
            return nil
        }
    }

}
