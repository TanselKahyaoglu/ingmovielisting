//
//  CustomJSONDecoder.swift
//  INGMovieAppTests
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

class CustomJSONDecoder {
    
    class func decoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
    
}
