//
//  MainPageVMTest.swift
//  INGMovieAppTests
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import XCTest

@testable import INGMovieApp

class MainPageVMTest: XCTestCase {

    //MARK: Variables
    var viewModel: MainPageVM?
    var service: MovieListServiceMock?
    
    override func setUp() {
        super.setUp()
        service = MovieListServiceMock()
        guard let service = service else { return }
        viewModel = MainPageVM(service: service)
    }

    func testInit() {
        XCTAssertNotNil(viewModel)
    }

    func testMovieListSuccess() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        XCTAssertTrue(viewModel?.getMovieCount() != 0)
    }
    
    func testMovieDetail() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        viewModel?.delegate = self
        viewModel?.getMovieDetail(at: IndexPath(row: 0, section: 0))

    }
    
    func testMovieSearch() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        viewModel?.search(with: "Historias")
        XCTAssertTrue(viewModel?.getMovieCount() == 1)
    }
    
    func testMovieClearSearch() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        viewModel?.search(with: "")
        XCTAssertTrue(viewModel?.getMovieCount() != 0)
    }
    
    func testAddFavorite() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        let movie = viewModel?.getMovie(at: IndexPath(row: 0, section: 0))
        viewModel?.addMovieToFavorites(movie: movie!)
        XCTAssertTrue(FavoritesHelper.checkIsFavorited(id: movie!.id))
    }
    
    func testRemoveFavorite() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        let movie = viewModel?.getMovie(at: IndexPath(row: 0, section: 0))
        viewModel?.removeMovieFromFavorites(movie: movie!)
        XCTAssertFalse(FavoritesHelper.checkIsFavorited(id: movie!.id))
    }
    
    func testNextPage() {
        service?.isFailTest = false
        viewModel?.getPopularMovies()
        viewModel?.nextPage()
        XCTAssertTrue(viewModel?.getMovieCount() == 2)
    }

    func testMovieListFailed() {
        service?.isFailTest = true
        viewModel?.getPopularMovies()
        XCTAssertTrue(viewModel?.getMovieCount() == 0)
    }

}

extension MainPageVMTest: MainPageVMDelegate {
    
    func moviesLoaded() { }
    
    func movieDetailLoaded(detail: MovieDetailResponse) {
        XCTAssertTrue(detail.id == 287947)
    }
    
    func showLoading() { }
    
    func hideLoading() { }
    
}
