//
//  MovieListService.swift
//  INGMovieAppTests
//
//  Created by Tansel Kahyaoglu on 5.12.2020.
//

import Foundation

@testable import INGMovieApp

class MovieListServiceMock: MovieListService {
    
    var isFailTest: Bool = false
    
    override func getMovieList(page: Int, onSuccess: @escaping Handler<GetMoviesResponse>, onFailure: @escaping Handler<BaseErrorResponse>) {
        if isFailTest {
           onFailure(BaseErrorResponse(statusCode: -1, statusMessage: "Error", success: false))
        } else {
            do {
                onSuccess(try CustomJSONDecoder.decoder().decode(GetMoviesResponse.self, from: JSONLoader.load(fileName: "MovieList")))
            } catch {
                onFailure(BaseErrorResponse(statusCode: -1, statusMessage: "Error", success: false))
            }
        }
    }
    
    override func getMovieDetail(id: Int, onSuccess: @escaping Handler<MovieDetailResponse>, onFailure: @escaping Handler<BaseErrorResponse>) {
        if isFailTest {
           onFailure(BaseErrorResponse(statusCode: -1, statusMessage: "Error", success: false))
        } else {
            do {
                onSuccess(try CustomJSONDecoder.decoder().decode(MovieDetailResponse.self, from: JSONLoader.load(fileName: "MovieDetail")))
            } catch {
                onFailure(BaseErrorResponse(statusCode: -1, statusMessage: "Error", success: false))
            }
        }
    }
    

    
}
